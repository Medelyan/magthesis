package com.gitlab.medelyan.magthesis.commands

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.types.file
import com.gitlab.medelyan.magthesis.BrightnessMethod.HSP
import com.gitlab.medelyan.magthesis.RgbPixel
import com.gitlab.medelyan.magthesis.extensions.ffmpeg.allocateRgbBuffers
import com.gitlab.medelyan.magthesis.extensions.ffmpeg.createCodecContext
import com.gitlab.medelyan.magthesis.extensions.ffmpeg.createFormatContext
import com.gitlab.medelyan.magthesis.extensions.ffmpeg.createFrame
import com.gitlab.medelyan.magthesis.extensions.ffmpeg.createSwsContext
import com.gitlab.medelyan.magthesis.extensions.ffmpeg.destroy
import com.gitlab.medelyan.magthesis.extensions.ffmpeg.frameType
import com.gitlab.medelyan.magthesis.extensions.ffmpeg.open
import com.gitlab.medelyan.magthesis.extensions.ffmpeg.readVideoFrames
import com.gitlab.medelyan.magthesis.extensions.ffmpeg.getRgbPixels
import com.gitlab.medelyan.magthesis.extensions.ffmpeg.scale
import com.gitlab.medelyan.magthesis.extensions.ffmpeg.setupFor
import com.gitlab.medelyan.magthesis.extensions.ffmpeg.videoChannels
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ClosedReceiveChannelException
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import org.bytedeco.ffmpeg.avcodec.AVCodecContext
import org.bytedeco.ffmpeg.avutil.AVFrame
import org.bytedeco.ffmpeg.global.avcodec
import org.bytedeco.javacpp.BytePointer
import java.io.FileOutputStream
import kotlin.math.min

private val logger = KotlinLogging.logger { }

class AnalyzeVideoCommand : CliktCommand(name = "analyze") {
    private val streamSdpFile by argument().file(
        mustExist = true,
        canBeDir = false,
        mustBeReadable = true,
    )

    override fun run() = runBlocking(Dispatchers.IO) {
        val context = createFormatContext()
        val codec = createCodecContext()

        try {
            avcodec.avcodec_register_all()
            context.open(streamSdpFile.absolutePath)

            val video = context.videoChannels.first()
            codec.setupFor(video)
            val (channel, jobs) = launchProcessors(codec)

            logger.debug { "Processing frames" }
            var videoFrames = 0
            context.readVideoFrames(codec).withIndex().forEach {
                videoFrames++
                runBlocking { channel.send(it.index to it.value) }
            }
            channel.close()
            jobs.forEach { it.join() }

            val frameRate = video.frameRate
            println(
                """
                    Resolution: ${video.width}x${video.height}
                    Video frames: $videoFrames
                    Frame rate: $frameRate, 
                    Calculated duration: ${videoFrames / frameRate} seconds
                """.trimIndent()
            )
        } finally {
            codec.destroy()
            context.destroy()
        }
    }

    private fun CoroutineScope.launchProcessors(codec: AVCodecContext): Pair<Channel<Pair<Int, AVFrame>>, MutableList<Job>> {
        val channel = Channel<Pair<Int, AVFrame>>(Channel.UNLIMITED)

        val jobs = mutableListOf<Job>()
        repeat(5) {
            jobs += launch { processFrame(channel, codec) }
        }
        return Pair(channel, jobs)
    }

    private suspend fun processFrame(channel: ReceiveChannel<Pair<Int, AVFrame>>, codec: AVCodecContext) = try {
        while (true) {
            val (i, frame) = channel.receive()
            val width = codec.width()
            val height = codec.height()
            val rgbFrame = createFrame().also { it.allocateRgbBuffers(width, height) }

            logger.debug { "Processing frame#$i" }
            try {
                val swsContext = codec.createSwsContext()
                swsContext.scale(frame, rgbFrame, codec)

                var pixels = rgbFrame.getRgbPixels(width, height)
                pixels = pixels.map { row ->
                    row.map {
                        val y = min(it.brightness(HSP).toInt(), 255)
                        RgbPixel(y, y, y)
                    }
                }

                logger.trace { "Frame type: ${frame.frameType()}" }

                FileOutputStream("dev/img$i.ppm").saveRgbImage(pixels)
            } finally {
                frame.destroy()
                rgbFrame.destroy()
            }
            logger.trace { "Frame#$i processed" }
        }
    } catch (e: ClosedReceiveChannelException) {
        // ok, ended
    }

    private fun FileOutputStream.saveRgbImage(data: List<List<RgbPixel>>) = use {
        val height = data.size
        val width = data[0].size
        val bytes = ByteArray(width * 3)
        it.write("P6\n$width $height\n255\n".toByteArray()) // header
        for (row in data) {
            for ((i, pixel) in row.withIndex()) {
                bytes[i * 3] = pixel.bytes[0]
                bytes[i * 3 + 1] = pixel.bytes[1]
                bytes[i * 3 + 2] = pixel.bytes[2]
            }
            it.write(bytes)
        }
    }
}

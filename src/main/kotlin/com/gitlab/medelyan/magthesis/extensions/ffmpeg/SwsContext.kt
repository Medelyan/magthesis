package com.gitlab.medelyan.magthesis.extensions.ffmpeg

import mu.KotlinLogging
import org.bytedeco.ffmpeg.avcodec.AVCodecContext
import org.bytedeco.ffmpeg.avutil.AVFrame
import org.bytedeco.ffmpeg.global.swscale
import org.bytedeco.ffmpeg.swscale.SwsContext

private val logger = KotlinLogging.logger { }

fun SwsContext.scale(source: AVFrame, dest: AVFrame, codec: AVCodecContext) {
    val height = swscale.sws_scale(
        this,
        source.data(),
        source.linesize(),
        0,
        codec.height(),
        dest.data(),
        dest.linesize(),
    )

    logger.trace { "Scaled, height: $height" }
}

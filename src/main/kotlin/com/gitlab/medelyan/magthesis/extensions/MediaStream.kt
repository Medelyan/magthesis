package com.gitlab.medelyan.magthesis.extensions

import org.bytedeco.ffmpeg.avformat.AVStream

class MediaStream(val stream: AVStream, val id: Int) {
    val width: Int
        get() = stream.codecpar().width()

    val height: Int
        get() = stream.codecpar().height()

    val frameRate: Double
        get() = stream.avg_frame_rate().num().toDouble() / stream.avg_frame_rate().den()
}
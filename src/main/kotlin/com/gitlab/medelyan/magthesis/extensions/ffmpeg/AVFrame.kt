package com.gitlab.medelyan.magthesis.extensions.ffmpeg

import com.gitlab.medelyan.magthesis.RgbPixel
import com.gitlab.medelyan.magthesis.extensions.throwIfError
import mu.KotlinLogging
import org.bytedeco.ffmpeg.avutil.AVFrame
import org.bytedeco.ffmpeg.global.avutil
import org.bytedeco.ffmpeg.global.avutil.AV_PIX_FMT_RGB24
import org.bytedeco.javacpp.BytePointer

private val logger = KotlinLogging.logger { }

fun createFrame(): AVFrame = avutil.av_frame_alloc() ?: error("Failed to allocate frame")

fun AVFrame.destroy() = avutil.av_frame_free(this)

fun AVFrame.frameType() = Char(avutil.av_get_picture_type_char(pict_type()).toInt())

fun AVFrame.allocateRgbBuffers(width: Int, height: Int) {
    val align = 1
    val bytes = avutil.av_image_get_buffer_size(AV_PIX_FMT_RGB24, width, height, align)
    bytes.throwIfError()
    val buffer = BytePointer(avutil.av_malloc(bytes.toLong()))
    avutil.av_image_fill_arrays(this.data(), this.linesize(), buffer, AV_PIX_FMT_RGB24, width, height, align)
}

fun AVFrame.getRgbPixels(width: Int, height: Int): List<List<RgbPixel>> {
    val pixels = ArrayList<ArrayList<RgbPixel>>(height)

    val bytes = ByteArray(height * width * 3)

    data(0).position(0).get(bytes)

    logger.trace { "Reading rgb pixels" }
    for (y in 0 until height) {
        logger.trace { "Reading line $y, ${width}x$height" }
        val row = ArrayList<RgbPixel>(width)

        logger.trace { "Converting bytes to rgb pixel" }
        val offset = y * 3 * width
        for (i in 0 until width) {
            row += RgbPixel.of(
                r = bytes[offset + i * 3],
                g = bytes[offset + i * 3 + 1],
                b = bytes[offset + i * 3 + 2],
            )
        }
        logger.trace { "Done converting bytes to rgb pixel" }

        pixels += row
    }
    logger.trace { "Done reading rgb pixels" }

    return pixels
}

package com.gitlab.medelyan.magthesis.extensions

import org.bytedeco.ffmpeg.global.avutil
import java.nio.charset.StandardCharsets

fun Int.throwIfError() {
    if (this < 0) error(this.toErrorMessage())
}

fun Int.toErrorMessage(): String {
    val bytes = ByteArray(200)
    avutil.av_strerror(this, bytes, bytes.size.toLong())
    return bytes.toString(StandardCharsets.UTF_8).substringBefore(Char(0))
}
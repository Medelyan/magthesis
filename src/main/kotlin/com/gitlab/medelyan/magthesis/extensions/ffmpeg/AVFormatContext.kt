package com.gitlab.medelyan.magthesis.extensions.ffmpeg

import com.gitlab.medelyan.magthesis.extensions.MediaStream
import com.gitlab.medelyan.magthesis.extensions.throwIfError
import mu.KotlinLogging
import org.bytedeco.ffmpeg.avcodec.AVCodecContext
import org.bytedeco.ffmpeg.avcodec.AVPacket
import org.bytedeco.ffmpeg.avformat.AVFormatContext
import org.bytedeco.ffmpeg.avutil.AVDictionary
import org.bytedeco.ffmpeg.avutil.AVFrame
import org.bytedeco.ffmpeg.global.avformat
import org.bytedeco.ffmpeg.global.avutil
import java.util.Queue
import java.util.concurrent.ConcurrentLinkedQueue

private val logger = KotlinLogging.logger {}

val AVFormatContext.videoChannels: List<MediaStream>
    get() = getStreamsOfType(this, avutil.AVMEDIA_TYPE_VIDEO)

val AVFormatContext.packets: Iterator<AVPacket>
    get() = object : Iterator<AVPacket> {
        private var lastPacket = AVPacket()

        override fun hasNext(): Boolean = avformat.av_read_frame(this@packets, lastPacket) >= 0

        override fun next(): AVPacket = lastPacket
    }

fun AVFormatContext.readVideoFrames(codec: AVCodecContext): Iterator<AVFrame> = object : Iterator<AVFrame> {
    private val frames: Queue<AVFrame> = ConcurrentLinkedQueue()
    private val packets: Iterator<AVPacket> = this@readVideoFrames.packets
    private lateinit var lastPacket: AVPacket

    private fun pollNextFrame(): Boolean {
        while (true) {
            if (this::lastPacket.isInitialized) lastPacket.free()

            if (!packets.hasNext()) return false

            lastPacket = packets.next()
            // if (lastPacket.stream_index() != this@readVideoFrames.videoChannels[0].id) continue

            val (r, frame) = codec.readFrame(lastPacket)

            if (r == 0) {
                frames += frame
                return true
            }

            when (r) {
                avutil.AVERROR_EAGAIN() -> continue
                avutil.AVERROR_EOF() -> return false
                else -> r.throwIfError()
            }
        }
    }

    override fun hasNext(): Boolean = pollNextFrame()

    override fun next(): AVFrame = frames.poll()
}

fun createFormatContext(): AVFormatContext = avformat.avformat_alloc_context()

fun AVFormatContext.open(fileName: String) {
    logger.debug { "Opening $fileName" }
    val options = AVDictionary()
    avutil.av_dict_set(options, "protocol_whitelist", "file,rtp,udp", 0)
    avformat.avformat_open_input(this, fileName, null, options).throwIfError()
    avformat.avformat_find_stream_info(this, null as? AVDictionary).throwIfError()
}

fun AVFormatContext.destroy() = avformat.avformat_close_input(this)

private fun getStreamsOfType(
    formatContext: AVFormatContext,
    typeId: Int,
): List<MediaStream> {
    val list = mutableListOf<MediaStream>()
    for (i in 0 until formatContext.nb_streams()) {
        if (formatContext.streams(i).codecpar().codec_type() == typeId) {
            list += MediaStream(formatContext.streams(i), i)
        }
    }

    require(list.isNotEmpty()) { "Video channels not found" }
    return list
}

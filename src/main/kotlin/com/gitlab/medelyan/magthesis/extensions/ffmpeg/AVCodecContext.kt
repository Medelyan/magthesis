package com.gitlab.medelyan.magthesis.extensions.ffmpeg

import com.gitlab.medelyan.magthesis.extensions.MediaStream
import com.gitlab.medelyan.magthesis.extensions.throwIfError
import mu.KotlinLogging
import org.bytedeco.ffmpeg.avcodec.AVCodec
import org.bytedeco.ffmpeg.avcodec.AVCodecContext
import org.bytedeco.ffmpeg.avcodec.AVPacket
import org.bytedeco.ffmpeg.avutil.AVDictionary
import org.bytedeco.ffmpeg.avutil.AVFrame
import org.bytedeco.ffmpeg.global.avcodec
import org.bytedeco.ffmpeg.global.avutil
import org.bytedeco.ffmpeg.global.swscale
import org.bytedeco.ffmpeg.swscale.SwsContext
import org.bytedeco.javacpp.DoublePointer

private val logger = KotlinLogging.logger { }

fun createCodecContext(): AVCodecContext = avcodec.avcodec_alloc_context3(null)

fun AVCodecContext.destroy() {
    avcodec.avcodec_close(this)
    avcodec.avcodec_free_context(this)
}

fun AVCodecContext.setupFor(video: MediaStream) {
    logger.debug { "Setting up video codec" }
    avcodec.avcodec_parameters_to_context(this, video.stream.codecpar()).throwIfError()
    val codec: AVCodec = avcodec.avcodec_find_decoder(this.codec_id()) ?: error("Decoder not found")
    avcodec.avcodec_open2(this, codec, null as? AVDictionary).throwIfError()
}

typealias ExitCode = Int

fun AVCodecContext.readFrame(packet: AVPacket): Pair<ExitCode, AVFrame> {
    avcodec.avcodec_send_packet(this, packet).throwIfError()
    val frame = createFrame()
    return avcodec.avcodec_receive_frame(this, frame) to frame
}

fun AVCodecContext.createSwsContext(targetPixFormat: Int = avutil.AV_PIX_FMT_RGB24): SwsContext =
    swscale.sws_getContext(
        width(),
        height(),
        pix_fmt(),
        width(),
        height(),
        targetPixFormat,
        swscale.SWS_BILINEAR,
        null,
        null,
        null as? DoublePointer,
    ) ?: error("Failed to create color transform context")

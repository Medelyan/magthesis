package com.gitlab.medelyan.magthesis.extensions.ffmpeg

import org.bytedeco.ffmpeg.avcodec.AVPacket
import org.bytedeco.ffmpeg.global.avcodec

fun AVPacket.free() = avcodec.av_packet_unref(this)
package com.gitlab.medelyan.magthesis

import com.gitlab.medelyan.magthesis.BrightnessMethod.COLOR_CONTRAST
import com.gitlab.medelyan.magthesis.BrightnessMethod.HSP
import com.gitlab.medelyan.magthesis.BrightnessMethod.RELATIVE_LUMINANCE
import kotlin.math.max
import kotlin.math.sqrt

enum class BrightnessMethod {
    /**
     * https://en.wikipedia.org/wiki/Relative_luminance
     */
    RELATIVE_LUMINANCE,

    /**
     * See https://www.w3.org/TR/AERT/#color-contrast
     */
    COLOR_CONTRAST,

    /**
     * See https://alienryderflex.com/hsp.html
     */
    HSP,
}

data class RgbPixel(
    val r: Int,
    val g: Int,
    val b: Int,
    val bytes: ByteArray = byteArrayOf(r.mapToByte(), g.mapToByte(), b.mapToByte()),
) {

    companion object {
        fun of(r: Byte, g: Byte, b: Byte): RgbPixel = RgbPixel(
            r = r.mapFrom0(),
            g = g.mapFrom0(),
            b = b.mapFrom0(),
            bytes = byteArrayOf(r, g, b)
        )
    }

    fun brightness(method: BrightnessMethod): Double = when (method) {
        RELATIVE_LUMINANCE -> {
            val normalR = r / 255
            val normalG = g / 255
            val normalB = b / 255
            0.2126 * normalR + 0.7152 * normalG + 0.0722 * normalB
        }
        COLOR_CONTRAST -> {
            val perceived = 0.299 * r + 0.587 * g + 0.114 * b
            max(perceived, 255.0)
        }
        HSP -> {
            sqrt(0.299 * r * r + 0.587 * g * g + 0.114 * b * b)
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is RgbPixel) return false

        if (r != other.r) return false
        if (g != other.g) return false
        if (b != other.b) return false
        if (!bytes.contentEquals(other.bytes)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = r
        result = 31 * result + g
        result = 31 * result + b
        result = 31 * result + bytes.contentHashCode()
        return result
    }
}

private fun Byte.mapFrom0(): Int = this.toInt() - Byte.MIN_VALUE.toInt()

private fun Int.mapToByte(): Byte = (this + Byte.MIN_VALUE.toInt()).toByte()
 
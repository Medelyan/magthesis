package com.gitlab.medelyan.magthesis

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.subcommands
import com.gitlab.medelyan.magthesis.commands.AnalyzeVideoCommand

class Main : CliktCommand() {
    override fun run() {}
}

fun main(vararg args: String) = Main().subcommands(AnalyzeVideoCommand()).main(args)
